using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HeroProperty : MonoBehaviour
{
    [SerializeField] private TMP_Text num;

    // 个位数
    public void Init(string number) {
        num.text = null;
        foreach(char i in number) {
            num.text += "<sprite name=\"" + i + "\">";
        }
    }
}
