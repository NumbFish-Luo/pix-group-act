using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GearColor : MonoBehaviour
{
    [SerializeField] private Image color1;
    [SerializeField] private Image color2;
    [SerializeField] private Image color3;

    public void Init(string colorString) {
        char[] colors = colorString.ToCharArray();
        color1.sprite = Resources.Load<Sprite>("UI/GearColor/" + colors[0]);
        color2.sprite = Resources.Load<Sprite>("UI/GearColor/" + colors[1]);
        color3.sprite = Resources.Load<Sprite>("UI/GearColor/" + colors[2]);
    }

}
