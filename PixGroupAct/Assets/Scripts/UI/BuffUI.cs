using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BuffUI : MonoBehaviour
{
    [SerializeField] private Image one;
    [SerializeField] private Image two;
    [SerializeField] private Image three;
    [SerializeField] private Image four;
    [SerializeField] private Image five;
    [SerializeField] private Image six;

    public void Init() {
        one.enabled = true;
        two.enabled = false;
        three.enabled = false;
        four.enabled = false;
        five.enabled = false;
        six.enabled = false;
        one.sprite = Resources.Load<Sprite>("UI/BuffIcon/出血");
    }

}
