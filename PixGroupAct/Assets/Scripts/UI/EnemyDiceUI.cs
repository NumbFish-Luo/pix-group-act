using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyDiceUI : MonoBehaviour
{
    public void Init(int[] diceList) {
        Image[] temp = this.transform.GetComponentsInChildren<Image>();
        for(int i = 1; i < temp.Length; i++) {
            temp[i].sprite = Resources.Load<Sprite>("UI/BasicDice_1/" + diceList[i - 1]);
        }
    }
}
