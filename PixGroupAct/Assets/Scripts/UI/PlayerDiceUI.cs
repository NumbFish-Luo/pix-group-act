using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDiceUI : MonoBehaviour
{
    [SerializeField] private GameObject dice;

    public void Init() {
        GameObject diceOne = Instantiate(dice, this.transform);
        GameObject diceTwo = Instantiate(dice, this.transform);
        GameObject diceThree = Instantiate(dice, this.transform);
        GameObject diceFour = Instantiate(dice, this.transform);

        diceOne.GetComponent<DiceUI>().Init();
        diceTwo.GetComponent<DiceUI>().Init();
        diceThree.GetComponent<DiceUI>().Init();
        diceFour.GetComponent<DiceUI>().Init(3, 14);

    }

}
