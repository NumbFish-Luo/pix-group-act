﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DiceUI : MonoBehaviour
{
    [SerializeField] private Image background;
    [SerializeField] private TMP_Text text;
    [SerializeField] private Button btn;


    private int randomNumber;
    private int num;
    private bool isSelect = false;

    // 初始化骰子
    public void Init() {
        background.enabled = false;
        text.enabled = false;
        btn.onClick.AddListener(ClickCurrentDice);
        RoleDice();
    }

    // 初始化骰子带数字
    public void Init(int diceLeft, int selectedDice) {
        this.num = diceLeft;
        this.randomNumber = selectedDice;

        background.enabled = true;
        text.enabled = true;
        btn.onClick.AddListener(ClickCurrentDice);

        text.text = InitNum(num + "");
        this.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/BattleDice_2/" + selectedDice);
    }

    // 掷骰子
    public void RoleDice() {
        randomNumber = Random.Range(1, 25);
        this.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/BattleDice_2/" + randomNumber);
    }

    // 获取目前骰子编号
    public int GetCurrentDice() {
        return this.randomNumber;
    }

    // 点击骰子
    public void ClickCurrentDice() {
        if (!isSelect) {
            this.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/BattleDice_1/" + randomNumber);
            isSelect = !isSelect;
        } else {
            this.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/BattleDice_2/" + randomNumber);
            isSelect = !isSelect;
        }
    }

    // 使用自选骰子
    public void UseDice() {
        if(num > 0) {
            num--;
            text.text = InitNum(num + "");
            // 自选骰子用完变灰
            if (num == 0)
                this.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/BattleDice_2/" + randomNumber);
        }
    }

    // 初始化属性数字
    public string InitNum(string num) {
        string temp = null;
        foreach (char i in num) {
            temp += "<sprite name=\"" + i + "\">";
        }
        return temp;
    }


}
