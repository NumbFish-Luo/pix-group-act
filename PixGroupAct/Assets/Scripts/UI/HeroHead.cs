﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HeroHead : MonoBehaviour {
    [SerializeField] private Image head;
    [SerializeField] private TMP_Text heroName;
    [SerializeField] private TMP_Text attackNum;
    [SerializeField] private TMP_Text healthNum;
    [SerializeField] private TMP_Text armorNum;
    [SerializeField] private TMP_Text speedNum;

    [SerializeField] private GameObject gearUpPrefab;
    [SerializeField] private GameObject gearDownPrefab;
    [SerializeField] private GameObject buffs;


    // 初始化英雄，图片，名字，属性
    public void Init(string headPath, string heroName, string attack, string health, string armor, string speed, string gearOneColor, string gearTwoColor) {
        head.sprite = Resources.Load<Sprite>(headPath);
        this.heroName.text = heroName;
        attackNum.text = InitNum(attack);
        healthNum.text = InitNum(health);
        armorNum.text = InitNum(armor);
        speedNum.text = InitNum(speed);
        InitGear(gearOneColor, gearTwoColor);

        GameObject buff = Instantiate(buffs, this.transform);
        buff.GetComponent<BuffUI>().Init();

    }

    // 初始化属性数字
    public string InitNum(string num) {
        string temp = null;
        foreach (char i in num) {
            temp += "<sprite name=\"" + i + "\">";
        }
        return temp;
    }

    // 初始化装备_装备1_装备2
    public void InitGear(string gearOneColor, string gearTwoColor) {
        GameObject gearUp = Instantiate(gearUpPrefab, this.transform);
        GameObject gearDown = Instantiate(gearDownPrefab, this.transform);

        gearUp.GetComponent<GearUI>().Init(gearOneColor);
        gearDown.GetComponent<GearUI>().Init(gearTwoColor);
    }
}