using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayOrderUI : MonoBehaviour
{
    [SerializeField] private GameObject small;
    [SerializeField] private GameObject medium;
    [SerializeField] private GameObject large;

    //Queue<Node> orderList = new Queue<Node>();
    ArrayList orderList = new ArrayList();
    private GameObject temp;

    Dictionary<int, List<Node>> curOrderDict = new Dictionary<int, List<Node>>();
    Dictionary<int, List<Node>> nextOrderDict = new Dictionary<int, List<Node>>();

    public void UpdateOrder() {
        //while(orderList.Peek() != null) {
        //    oldList.Add(orderList.Dequeue());
        //}

        // Tester
        orderList.Add(new Node(0, "Monster/双子强盗/双子强盗迷你头像"));
        orderList.Add(new Node(1, "Monster/史莱姆/史莱姆迷你头像"));
        orderList.Add(new Node(1, "Monster/巨型史莱姆/巨型史莱姆迷你头像"));
        orderList.Add(new Node(1, "Monster/资源堆/资源堆迷你头像"));
        orderList.Add(new Node(2, "Monster/车夫/车夫迷你头像"));
        orderList.Add(new Node(3, "Monster/鱼叉弩炮/鱼叉弩炮迷你头像"));
        orderList.Add(new Node(3, "Monster/双子强盗/双子强盗迷你头像"));

        
        // 计算当前回合行动列表 //////////////////////////////////////
        List<Node> nodes = new List<Node> {
        new Node("Monster/双子强盗/双子强盗迷你头像", 10),
        new Node("Monster/史莱姆/史莱姆迷你头像", 10),
        new Node("Monster/巨型史莱姆/巨型史莱姆迷你头像", 8),
        new Node("Monster/资源堆/资源堆迷你头像", 8),
        new Node("Monster/车夫/车夫迷你头像", 6),
        new Node("Monster/鱼叉弩炮/鱼叉弩炮迷你头像", 5),
        new Node("Monster/双子强盗/双子强盗迷你头像", 5)}; // 假设这里有数据, 有n个节点
        nodes.Sort((Node x, Node y) => {
            return x.speed > y.speed ? -1 : 1;
        }); // 反向排序
            // 按行动速度从大到小遍历
        int preSpeed = -1;
        int curOrder = -1;
        curOrderDict.Clear(); // 清空当前行动顺序
        foreach (Node n in nodes) {
            if (preSpeed != n.speed) { // 速度相同
                ++curOrder;
            }
            preSpeed = n.speed;
            if (curOrderDict.TryGetValue(curOrder, out List<Node> nodeList) == false) {
                curOrderDict.Add(curOrder, new List<Node>());
                nodeList = curOrderDict[curOrder];
            }
            nodeList.Add(n);
        }
        // 输出结果
        foreach (KeyValuePair<int, List<Node>> o in curOrderDict) {
            int order = o.Key;
            List<Node> nodeList = o.Value;
            Debug.Log($"order: { order }");
            foreach (Node n in nodeList) {
                Debug.Log($"\tnode: { n.GetPath() }, { n.speed }");
            }
        }

        // 计算下一回合行动列表 ////////////////////////////
        List<Node> nodeList0 = curOrderDict[0];
        // 取出第一个节点
        Node node = nodeList0[0];
        // node.Action();
        // 存入下一回合列表
        nodeList0.Remove(node); // 已行动, 进行移除
        

        for (int i = orderList.Count - 1; i >= 0; i--) {
            if (((Node)orderList[i]).GetOrder() == 0) {
                temp = Instantiate(large, this.transform);
                temp.transform.Find("头像").GetComponent<Image>().sprite = Resources.Load<Sprite>(((Node)orderList[i]).GetPath());
                orderList.Remove(i);
            }
            else if(((Node)orderList[i]).GetOrder() == 1) {
                temp = Instantiate(medium, this.transform);
                temp.transform.Find("头像").GetComponent<Image>().sprite = Resources.Load<Sprite>(((Node)orderList[i]).GetPath());
                ((Node)orderList[i]).MoveOn();
            }
            else if(((Node)orderList[i]).GetOrder() > 1) {
                temp = Instantiate(small, this.transform);
                temp.transform.Find("头像").GetComponent<Image>().sprite = Resources.Load<Sprite>(((Node)orderList[i]).GetPath());
                ((Node)orderList[i]).MoveOn();
            }
        }
    }
}

class Node {
    private int order;
    private string path;
    public int speed;

    public Node(int o, string p) {
        this.order = o;
        this.path = p;
        this.speed = 0;
    }

    public Node(string p, int s) {
        order = -1;
        path = p;
        speed = s;
    }

    public int GetOrder() {
        return this.order;
    }
    public string GetPath() {
        return this.path;
    }

    public void MoveOn() {
        order--;
    }
}