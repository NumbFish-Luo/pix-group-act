﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHead : MonoBehaviour {
    [SerializeField] private Image head;
    [SerializeField] private TMP_Text enemyName;
    [SerializeField] private TMP_Text attackNum;
    [SerializeField] private TMP_Text healthNum;
    [SerializeField] private TMP_Text armorNum;
    [SerializeField] private TMP_Text speedNum;

    [SerializeField] private Image gearUp;
    [SerializeField] private Image gearDown;
    [SerializeField] private GameObject buffs;


    // 初始化敌人，图片，名字，属性
    public void Init(string headPath, string name, string attack, string health, string armor, string speed) {
        head.sprite = Resources.Load<Sprite>(headPath);
        enemyName.text = name;
        attackNum.text = InitNum(attack);
        healthNum.text = InitNum(health);
        armorNum.text = InitNum(armor);
        speedNum.text = InitNum(speed);

        GameObject buff = Instantiate(buffs, this.transform);
        buff.GetComponent<BuffUI>().Init();

    }

    // 初始化敌人，图片，名字，属性，装备1
    public void Init(string headPath, string name, string attack, string health, string armor, string speed, string gearUpPath) {
        head.sprite = Resources.Load<Sprite>(headPath);
        enemyName.text = name;
        attackNum.text = InitNum(attack);
        healthNum.text = InitNum(health);
        armorNum.text = InitNum(armor);
        speedNum.text = InitNum(speed);

        gearUp.sprite = Resources.Load<Sprite>(gearUpPath);

        GameObject buff = Instantiate(buffs, this.transform);
        buff.GetComponent<BuffUI>().Init();

    }

    // 初始化敌人，图片，名字，属性，装备1，装备2
    public void Init(string headPath, string name, string attack, string health, string armor, string speed, string gearUpPath, string gearDownPath) {
        head.sprite = Resources.Load<Sprite>(headPath);
        enemyName.text = name;
        attackNum.text = InitNum(attack);
        healthNum.text = InitNum(health);
        armorNum.text = InitNum(armor);
        speedNum.text = InitNum(speed);

        gearUp.sprite = Resources.Load<Sprite>(gearUpPath);
        gearDown.sprite = Resources.Load<Sprite>(gearDownPath);

        GameObject buff = Instantiate(buffs, this.transform);
        buff.GetComponent<BuffUI>().Init();

    }

    // 初始化属性数字
    public string InitNum(string num) {
        string temp = null;
        foreach (char i in num) {
            temp += "<sprite name=\"" + i + "\">";
        }
        return temp;
    }
}