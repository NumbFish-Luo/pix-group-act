﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GearUI : MonoBehaviour
{
    [SerializeField] private GameObject gearFrame;
    [SerializeField] private GameObject emptyFrame;

    public void Init(string gearColor) {
        gearFrame.SetActive(true);
        if (gearColor != null && gearColor != "") {
            gearFrame.transform.Find("Colors").GetComponent<GearColor>().Init(gearColor);
        } else {
            // 加空装备框图
        }
    }

}
