using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoSingleton<GameManager> {
    [SerializeField] private GameObject heroHeadPrefab;
    [SerializeField] private GameObject enemyHeadPrefab;

    [SerializeField] private Transform hero1;
    [SerializeField] private Transform hero2;
    [SerializeField] private Transform hero3;
    //private List<HeroHead> heroHeads;

    [SerializeField] private Transform enemy1;
    [SerializeField] private Transform enemy2;
    [SerializeField] private Transform enemy3;
    [SerializeField] private Transform enemy4;
    //private List<EnemyHead> enemyHeads;

    [SerializeField] private GameObject orderUI;
    [SerializeField] private GameObject playerDiceUI;
    [SerializeField] private GameObject enemyDiceUI;


    private void Awake() {
        // 初始化英雄1
        GameObject heroHead1 = Instantiate(heroHeadPrefab, hero1);
        heroHead1.GetComponent<HeroHead>().Init("Hero/牧师头像", "", "7-8", "56", "123", "1", "RGB", null);

        // 初始化英雄2
        GameObject heroHead2 = Instantiate(heroHeadPrefab, hero2);
        heroHead2.GetComponent<HeroHead>().Init("Hero/ABC头像", "", "7-8", "56", "123", "1", null, null);

        // 初始化英雄3
        GameObject heroHead3 = Instantiate(heroHeadPrefab, hero3);
        heroHead3.GetComponent<HeroHead>().Init("Hero/DEF头像", "", "7-8", "56", "123", "1", "BGR", "PGP");

        // 初始化敌人1
        GameObject enemyHead1 = Instantiate(enemyHeadPrefab, enemy1);
        enemyHead1.GetComponent<EnemyHead>().Init("Monster/双子强盗/双子强盗头像", "", "7-8", "56", "123", "1");

        // 初始化敌人2
        GameObject enemyHead2 = Instantiate(enemyHeadPrefab, enemy2);
        enemyHead2.GetComponent<EnemyHead>().Init("Monster/巨型史莱姆/巨型史莱姆头像", "", "7-8", "56", "123", "1");

        // 初始化敌人3
        GameObject enemyHead3 = Instantiate(enemyHeadPrefab, enemy3);
        enemyHead3.GetComponent<EnemyHead>().Init("Monster/车夫/车夫头像", "", "7-8", "56", "123", "1");

        // 初始化敌人4
        GameObject enemyHead4 = Instantiate(enemyHeadPrefab, enemy4);
        enemyHead4.GetComponent<EnemyHead>().Init("Monster/鱼叉弩炮/鱼叉弩炮头像", "", "7-8", "56", "123", "1");


        // 初始化战斗顺序UI
        orderUI.GetComponent<PlayOrderUI>().UpdateOrder();

        // 初始化玩家骰子UI
        playerDiceUI.GetComponent<PlayerDiceUI>().Init();

        // 初始化敌人骰子UI
        enemyDiceUI.GetComponent<EnemyDiceUI>().Init(new int[] {1, 2, 3, 4});
    }
}
