using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using System.Reflection;

public class CfgHero {
    public int id;
    public string name;
    public int minAtk;
    public int maxAtk;
    public int def;
    public int hp;
    public int spd;
    public int defaultGearId;
}

public class CfgGear {
    public int id;
    public string name;
}

public class CfgManager : MonoSingleton<CfgManager> {
    private const string csvPathHero = "Cfg/Hero.csv";
    private const string csvPathGear = "Cfg/Gear.csv";

    public Dictionary<int, CfgHero> cfgHero;
    public Dictionary<int, CfgGear> cfgGear;

    public void Awake() {
        cfgHero = ReadCsv<CfgHero>(csvPathHero);
        cfgGear = ReadCsv<CfgGear>(csvPathGear);
    }

    private Dictionary<int, T> ReadCsv<T>(string filePath) where T : new() {
        // GB2312: codepage = 936
        string[] lines = File.ReadAllLines(filePath, Encoding.GetEncoding(936));

        // key在第0行, type在第1行, 中文注释在第2行
        string[] keys = lines[0].Split(',', '\t');
        string[] types = lines[1].Split(',', '\t');

        FieldInfo[] fields = typeof(T).GetFields();
        Dictionary<string, FieldInfo> fieldDict = new Dictionary<string, FieldInfo>();
        foreach (FieldInfo f in fields) {
            fieldDict.Add(f.Name, f);
        }

        Dictionary<int, T> result = new Dictionary<int, T>();
        // 数据从第3行开始
        for (int i = 3; i < lines.Length; ++i) {
            string[] values = lines[i].Split(',', '\t');
            T t = new T();
            for (int j = 0; j < values.Length; ++j) {
                FieldInfo field = fieldDict[keys[j]];
                string type = types[j];
                switch (type) {
                case "int":
                    field.SetValue(t, int.Parse(values[j]));
                    break;
                case "float":
                    field.SetValue(t, float.Parse(values[j]));
                    break;
                case "bool":
                    field.SetValue(t, bool.Parse(values[j]));
                    break;
                default:
                    field.SetValue(t, values[j]);
                    break;
                }
            }
            // values[0]固定为id
            result.Add(int.Parse(values[0]), t);
        }
        return result;
    }

    private string ToString<T>(T t) {
        FieldInfo[] fields = typeof(T).GetFields();
        string result = "";
        foreach (FieldInfo f in fields) {
            result += f.Name + ": " + f.GetValue(t) + "\n";
        }
        return result;
    }
}
